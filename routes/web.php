<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('foo', function () {
    $host = request()->getHost();
    $host = explode('.', $host, 2);
    return 'host: '. $host[0];
});

Route::get('constants', function() {
    $output = 'GOOGLE_CLOUD_PROJECT_ID' . env('GOOGLE_CLOUD_PROJECT_ID','not set') . PHP_EOL;
    $output .='GOOGLE_CLOUD_KEY_FILE' . env('GOOGLE_CLOUD_KEY_FILE', 'not set')  . PHP_EOL;
    $output .= 'GOOGLE_CLOUD_STORAGE_BUCKET' . env('GOOGLE_CLOUD_STORAGE_BUCKET', 'not set') . PHP_EOL;
    $output .= 'GOOGLE_CLOUD_STORAGE_PATH_PREFIX' . env('GOOGLE_CLOUD_STORAGE_PATH_PREFIX', 'not set') . PHP_EOL;
    $output .= 'GOOGLE_CLOUD_STORAGE_API_URI' . env('GOOGLE_CLOUD_STORAGE_API_URI', 'not set') . PHP_EOL;
    return $output;
});

Route::get('test', function() {
    $host = request()->getHost();
    $host = explode('.', $host, 2);
    try {
        $disk = Storage::disk('gcs');
        $url = $disk->get($host[0].'/.env');
        $image = $disk->url($host[0] .'/avatar.jpg');
    }catch (Exception $e) {
        echo $e->getMessage();
    }
    return view('test', ['name' => $url, 'avatar' => $image ]);
});
